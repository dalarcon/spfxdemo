import * as React from 'react';
import {  withStyles, Theme, createStyles, makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import styles from './Newcapex.module.scss';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles({
    root: {
      width: '100%',
      overflowX: 'auto',
    },
    table: {
      minWidth: 650,
    },
  });

  function createData(Status: string, UserName: string, TaskDateCreated: string, DateOfCompletedTask: string) {
    return { Status, UserName, TaskDateCreated, DateOfCompletedTask };
  }
  const Traking =  [{status:"00",Username:"aa",TaskDateCreated:"",DateOfCompletedTask:""},{status:"01",Username:"aa",TaskDateCreated:"AAA",DateOfCompletedTask:"AAA"}];
  const Traking2 = 
  "[{'status':'00. Draft','UserName':'Sharepoint User1','TaskDateCreated':'30/07/2019 15:12:57','DateOfCompletedTask':'30/07/2019 15:14:39'},"+
  "{'status':'01. Revision of Manager Requester','UserName':'Balbin, David','TaskDateCreated':'30/07/2019 15:14:51','DateOfCompletedTask':'30/07/2019 15:19:14'},"+
  "{'status':'02. Revision of Execution Area','UserName':'Sharepoint User4','TaskDateCreated':'30/07/2019 15:19:18','DateOfCompletedTask':'30/07/2019 15:50:26'},"+
  "{'status':'03. Revision of Central Manager Requester','UserName':'Sharepoint User3','TaskDateCreated':'30/07/2019 15:50:31','DateOfCompletedTask':'30/07/2019 15:58:33'}"+
  "]";


  const rows = [
    createData('00.  Draft', "Sharepoint User1", "30/07/2019 15:12:57", "30/07/2019 15:14:39"),
    createData('01.  Revision of Manager Requester', "Balbin, David","30/07/2019 15:14:51", "30/07/2019 15:19:14"),
    createData('02.  Revision of Execution Area', "Sharepoint User4", "30/07/2019 15:19:18", "30/07/2019 15:50:26"),
    createData('03.  Revision of Central Manager Requester',"Sharepoint User3", "30/07/2019 15:50:31", "30/07/2019 15:58:33")    
  ];

  

import { 
    ObtenerFuncionalidad,
    ObtenerTipoInversion,
    ObtenerTipoCategoria,
    ObtenerTipoRevision,
    ObtenerTipoRecomendacion,
    ObtenerUsuarioActual,
    ObtenerDocumentos,
    SubirDocumentos
 } from './RestAPIFunc';

import {
    DatePicker,
    TextField,
    Label,
    ComboBox,
    IComboBox,
    IComboBoxOption,
    IComboBoxProps,
    SelectableOptionMenuItemType,
    ChoiceGroup,
    FontWeights,
    PrimaryButton,
    IconButton,
    IIconProps
} from 'office-ui-fabric-react';
import "bootstrap/dist/css/bootstrap.css";
import { fontWeight } from '@material-ui/system';

interface TabPanelProps {
    children?: React.ReactNode;
    index: any;
    value: any;
}

function TabPanel(props: TabPanelProps) {
    const { children, value, index, ...other } = props;

    return (
        <Typography
            component="div"
            role="tabpanel"
            hidden={value !== index}
            id={`scrollable-auto-tabpanel-${index}`}
            aria-labelledby={`scrollable-auto-tab-${index}`}
            {...other}
        >
            <Box >{children}</Box>
        </Typography>
    );
}

function a11yProps(index: any) {
    return {
        id: `scrollable-auto-tab-${index}`,
        'aria-controls': `scrollable-auto-tabpanel-${index}`,
    };
}



const INITIAL_OPTIONS: IComboBoxOption[] = [
    { key: 'A', text: 'Option A' },
    { key: 'B', text: 'Option B' },
    { key: 'C', text: 'Option C' },
    { key: 'D', text: 'Option D' }
];

const MONTH_OPTIONS: IComboBoxOption[] = [
    { key: '0', text: 'Jan' },
    { key: '1', text: 'Feb' },
    { key: '2', text: 'Mar' },
    { key: '3', text: 'Apr' },
    { key: '3', text: 'May' },
    { key: '3', text: 'Jun' },
    { key: '3', text: 'Jul' },
    { key: '3', text: 'Aug' },
    { key: '3', text: 'Sep' },
    { key: '3', text: 'Oct' },
    { key: '3', text: 'Nov' },
    { key: '3', text: 'Dec' },
];

const YEARS_OPTIONS: IComboBoxOption[] = [
    { key: '2018', text: '2018' },
    { key: '2019', text: '2019' },
    { key: '2020', text: '2020' },
    { key: '2021', text: '2021' }
];






export class Subtitulo extends React.Component<any, any>{

    public render() {
        return (
            <div className={styles.subTitle} >
                <h5>{this.props.nombre}</h5>
            </div>
        );
    }
}

class GeneralInformation extends React.Component {
    render() {
        return (
            <div className="container" style={{ paddingTop: "3%" }}>
                <Subtitulo nombre="I. General Information"></Subtitulo>
                <div className="row">
                    <div className="col-6 col-md-6">
                        <Label>01. Request Code:</Label>
                    </div>
                    <div className="col-6 col-md-6">
                        <TextField underlined disabled value="CR-TI-2019-0028" onChanged={e => { this.setState({ RequestCode: e }); }} />
                    </div>
                </div>
                <div className="row">
                    <div className="col-6 col-md-6">
                        <Label>02. Date of Request:</Label>
                    </div>
                    <div className="col-6 col-md-6">
                        <DatePicker placeholder="Select a date..." ariaLabel="Select a date" />
                    </div>

                </div>
                <div className="row">
                    <div className="col-6 col-md-6"></div>
                    <div className="col-6 col-md-6">
                        <ChoiceGroup styles={{ flexContainer: { display: "flex" } }}
                            className="defaultChoiceGroup"
                            options={[
                                {
                                    key: 'In date',
                                    text: 'In date'
                                },
                                {
                                    key: 'Out of date',
                                    text: 'Out of date'
                                }
                            ]}
                            required={true}
                        />
                    </div>
                </div>
                
                <div className="row">
                    <div className="col-6 col-md-6">
                        <Label> 03. Status:</Label>
                    </div>
                    <div className="col-6 col-md-6">
                        <TextField underlined disabled value="01. Revision of Manager Requester" onChanged={e => { this.setState({ RequestCode: e }); }} />
                    </div>
                </div>
                <div className="row">
                    <div className="col-6 col-md-6">
                        <Label> 04. Year of Investment Program:</Label>
                    </div>
                    <div className="col-6 col-md-6">
                        <ComboBox
                            defaultSelectedKey="C"                            
                            autoComplete="on"
                            options={YEARS_OPTIONS}
                        />
                    </div>
                </div>
                <div className="row">
                    <div className="col-6 col-md-6">
                        <Label> 05. The request has an approved Budget in the Investment Plan:</Label>
                    </div>
                    <div className="col-6 col-md-6">
                        <ChoiceGroup styles={{ flexContainer: { display: "flex" } }}
                            className="defaultChoiceGroup"
                            options={[
                                {
                                    key: 'Yes',
                                    text: 'Yes'
                                },
                                {
                                    key: 'No',
                                    text: 'No'
                                }
                            ]}
                            required={true}
                        />
                    </div>
                </div>
            </div>
        )
    }
}

class RequestInformation extends React.Component <any,any> {

    constructor(props){
        super(props);
        this.state={
            INVTYPE_OPTIONS:[],
            INVCAT_OPTIONS:[]
        }

        ObtenerTipoInversion().then(result=>this.setState({INVTYPE_OPTIONS : result}))
        ObtenerTipoCategoria().then(result=>this.setState({INVCAT_OPTIONS : result}))
    }

    private _onFormatDate = (date: Date): string => {
        return date.getDate() + '/' + (date.getMonth() + 1) + '/' + (date.getFullYear() % 100);
    };

    render() {
        return (
            <div className="container" style={{ paddingTop: "3%" }}>
                <Subtitulo nombre="II. Request Information"></Subtitulo>
                <div className="row">
                    <div className="col-6 col-md-6">
                        <Label>01. Requester:</Label>
                    </div>
                    <div className="col-6 col-md-6">
                        <TextField underlined onChanged={e => { this.setState({ RequestCode: e }); }} />
                    </div>
                </div>
                <div className="row">
                    <div className="col-6 col-md-6">
                        <Label>02. Manager Requester:</Label>
                    </div>
                    <div className="col-6 col-md-6">
                        <TextField underlined onChanged={e => { this.setState({ RequestCode: e }); }} />
                    </div>
                </div>
                <div className="row">
                    <div className="col-6 col-md-6">
                        <Label>03. Manager Requester Department:</Label>
                    </div>
                    <div className="col-6 col-md-6">
                        <TextField underlined onChanged={e => { this.setState({ RequestCode: e }); }} />
                    </div>
                </div>
                <div className="row">
                    <div className="col-6 col-md-6">
                        <Label>04. Central Manager Requester:</Label>
                    </div>
                    <div className="col-6 col-md-6">
                        <TextField underlined onChanged={e => { this.setState({ RequestCode: e }); }} />
                    </div>
                </div>
                <div className="row">
                    <div className="col-6 col-md-6">
                        <Label>05. Name of the Investment:</Label>
                    </div>
                    <div className="col-6 col-md-6">
                        <TextField underlined onChanged={e => { this.setState({ RequestCode: e }); }} />
                    </div>
                </div>
                <div className="row">
                    <div className="col-6 col-md-6">
                        <Label>06. Investment Type:</Label>
                    </div>
                    <div className="col-6 col-md-6">
                        <ComboBox
                            options={this.state.INVTYPE_OPTIONS}                            
                        />
                    </div>
                </div>
                <div className="row">
                    <div className="col-6 col-md-6">
                        <Label>07. Investment Category:</Label>
                    </div>
                    <div className="col-6 col-md-6">
                        <ComboBox
                            options={this.state.INVCAT_OPTIONS}                            
                        />
                    </div>
                </div>
                <div className="row">
                    <div className="col-6 col-md-6">
                        <Label>08. Proposal date for operation:</Label>
                    </div>

                    <div className="col-2 col-md-2">
                        <ComboBox
                            options={MONTH_OPTIONS}
                        />
                    </div>
                    <div className="col-2 col-md-2">
                        <ComboBox
                            options={YEARS_OPTIONS}
                        />
                    </div>
                </div>
                <div className="row">
                    <div className="col-6 col-md-6">
                        <Label>09. Proposal date for operation:</Label>
                    </div>
                    <div className="col-6 col-md-6">
                        <TextField underlined onChanged={e => { this.setState({ RequestCode: e }); }} />
                    </div>
                </div>
                <div className="row">
                    <div className="col-6 col-md-6">
                        <Label>10. Justification (opportunity or problem):</Label>
                    </div>
                    <div className="col-6 col-md-6">
                        <TextField underlined onChanged={e => { this.setState({ RequestCode: e }); }} />
                    </div>
                </div>
                <div className="row">
                    <div className="col-6 col-md-6">
                        <Label>11. Risk of no execution:</Label>
                    </div>
                    <div className="col-6 col-md-6">
                        <TextField underlined onChanged={e => { this.setState({ RequestCode: e }); }} />
                    </div>
                </div>
            </div>
        )
    }
}

class ProposedSolutionForExecutionArea extends React.Component<any, any> {

    private listFunc1: Object[];
    private listFunc2: Object[];
    private listFunc3: Object[];
    private listFunc4: Object[];

    constructor(props) {
        super(props);
        this.state = {
            FUNC1_OPTIONS: [],
            FUNC2_OPTIONS: [],
            FUNC3_OPTIONS: [],
            FUNC4_OPTIONS: [],

        };   
        ObtenerFuncionalidad(1).then(
            result=>{
                this.listFunc1 = result;
                var resultado = this.obtenerFuncRelacionadas(0,2);
                this.setState({FUNC1_OPTIONS:resultado})
            }
        )        
        ObtenerFuncionalidad(2).then(result=>this.listFunc2 = result);
        ObtenerFuncionalidad(3).then(result=>this.listFunc3 = result);
        ObtenerFuncionalidad(4).then(result=>this.listFunc4 = result);



    }
    componentDidMount() {
    }

    private selectFuncionalidad(event: React.FormEvent<IComboBox>, option?: IComboBoxOption, index?: number, value?: string, cmbFunc?: number) {
        
        const opcionSelect = option.key;
        const valores = this.obtenerFuncRelacionadas(cmbFunc,opcionSelect);

        switch (cmbFunc+1) {
            case 2: this.setState({FUNC2_OPTIONS:valores});
                    this.setState({FUNC3_OPTIONS:[]})
                    this.setState({FUNC4_OPTIONS:[]})
                    break;
            case 3: this.setState({FUNC3_OPTIONS:valores});
                    this.setState({FUNC4_OPTIONS:[]})
                    break;
            case 4: this.setState({FUNC4_OPTIONS:valores});
                    break;
        }
    };

    private obtenerFuncRelacionadas(cmbFunc, valor) {
        var res;
        switch (cmbFunc + 1) {
            case 1: res = this.listFunc1.filter(d => d["PIAnioId"] == valor).map(f => ({ key: f["ID"], text: f["Title"] }));
                break;
            case 2: res = this.listFunc2.filter(d => d["Funcionalidad1Id"] === valor).map(f => ({ key: f["ID"], text: f["Title"] }));
                break;
            case 3: res = this.listFunc3.filter(d => d["Funcionalidad2Id"] === valor).map(f => ({ key: f["ID"], text: f["Title"] }));
                break;
            case 4: res = this.listFunc4.filter(d => d["Funcionalidad3Id"] === valor).map(f => ({ key: f["ID"], text: f["Title"] }));
                break;
        }

        return res;

    }
    
    render() {
        return (
            <div className="container" style={{ paddingTop: "3%" }}>
                <Subtitulo nombre="III. Proposed Solution for Execution Area"></Subtitulo>
                <div className="row">
                    <div className="col-6 col-md-6">
                        <Label>01. Project Manager:</Label>
                    </div>
                    <div className="col-6 col-md-6">
                        <TextField underlined onChanged={e => { this.setState({ RequestCode: e }); }} />
                    </div>
                </div>
                <div className="row">
                    <div className="col-6 col-md-6">
                        <Label>02. Execution Department:</Label>
                    </div>
                    <div className="col-6 col-md-6">
                        <TextField underlined onChanged={e => { this.setState({ RequestCode: e }); }} />
                    </div>
                </div>
                <div className="row">
                    <div className="col-6 col-md-6">
                        <Label>03. Functionality Level 1:</Label>
                    </div>
                    <div className="col-6 col-md-6">
                        <ComboBox
                            options={this.state.FUNC1_OPTIONS}
                            onChange={(event, option, index, value) => this.selectFuncionalidad(event, option, index, value, 1)}
                        />
                    </div>
                </div>
                <div className="row">
                    <div className="col-6 col-md-6">
                        <Label>04. Functionality Level 2:</Label>
                    </div>
                    <div className="col-6 col-md-6">
                        <ComboBox
                            options={this.state.FUNC2_OPTIONS}
                            onChange={(event, option, index, value) => this.selectFuncionalidad(event, option, index, value, 2)}
                        />
                    </div>
                </div>
                <div className="row">
                    <div className="col-6 col-md-6">
                        <Label>05. Functionality Level 3:</Label>
                    </div>
                    <div className="col-6 col-md-6">
                        <ComboBox
                            options={this.state.FUNC3_OPTIONS}
                            onChange={(event, option, index, value) => this.selectFuncionalidad(event, option, index, value, 3)}
                        />
                    </div>
                </div>
                <div className="row">
                    <div className="col-6 col-md-6">
                        <Label>06. Functionality Level 4:</Label>
                    </div>
                    <div className="col-6 col-md-6">
                        <ComboBox
                            options={this.state.FUNC4_OPTIONS}
                        />
                    </div>
                </div>
                <div className="row">
                    <div className="col-6 col-md-6">
                        <Label>07. Scope:</Label>
                    </div>
                    <div className="col-6 col-md-6">
                        <TextField underlined onChanged={e => { this.setState({ RequestCode: e }); }} />
                    </div>
                </div>
                <div className="row">
                    <div className="col-6 col-md-6">
                        <Label>08. Investment Estimation ($):</Label>
                    </div>
                    <div className="col-6 col-md-6">
                        <TextField underlined onChanged={e => { this.setState({ RequestCode: e }); }} />
                    </div>
                </div>
                <div className="row">
                    <div className="col-6 col-md-6">
                        <Label>09. Stakeholders:</Label>
                    </div>
                    <div className="col-6 col-md-6">
                        <TextField underlined onChanged={e => { this.setState({ RequestCode: e }); }} />
                    </div>
                </div>
            </div>
        )
    }
}

class ExecutionSchedule extends React.Component {
    render() {
        return (
            <div>
                <div className="container" style={{ paddingTop: "3%" }}>
                    <Subtitulo nombre="IV. Execution Schedule"></Subtitulo>
                    <table style={{width:"100%"}}>
                    <tr>
                        <th>Jan</th>
                        <th>Feb</th>
                        <th>Mar</th>
                        <th>Apr</th>
                        <th>May</th>
                        <th>Jun</th>
                        <th>Jul</th>
                        <th>Aug</th>
                        <th>Sep</th>
                        <th>Oct</th>
                        <th>Nov</th>
                        <th>Dec</th>
                        <th>P.I.</th>
                    </tr>
                    <tr>
                        <td><TextField underlined /></td>
                        <td><TextField underlined /></td>
                        <td><TextField underlined /></td>
                        <td><TextField underlined /></td>
                        <td><TextField underlined /></td>
                        <td><TextField underlined /></td>
                        <td><TextField underlined /></td>
                        <td><TextField underlined /></td>
                        <td><TextField underlined /></td>
                        <td><TextField underlined /></td>
                        <td><TextField underlined /></td>
                        <td><TextField underlined /></td>
                        <td>0</td>
                    </tr>
                    </table>
                    <table style={{width:"100%"}}>
                    <tr>
                        <th>Year 01</th>
                        <th>Year 02</th>
                        <th>Year 03</th>
                        <th>Year 04</th>
                        <th>Year 05</th>
                        <th>Year 06</th>
                        <th>Year 07</th>
                        <th>Year 08</th>
                        <th>Year 09</th>
                        <th>Year 10</th>
                        <th>Year 11</th>
                        <th>Year 12</th>
                        <th>Total</th>
                    </tr>
                    <tr>
                        <td><TextField underlined /></td>
                        <td><TextField underlined /></td>
                        <td><TextField underlined /></td>
                        <td><TextField underlined /></td>
                        <td><TextField underlined /></td>
                        <td><TextField underlined /></td>
                        <td><TextField underlined /></td>
                        <td><TextField underlined /></td>
                        <td><TextField underlined /></td>
                        <td><TextField underlined /></td>
                        <td><TextField underlined /></td>
                        <td><TextField underlined /></td>
                        <td>0</td>
                    </tr>
                    </table>                    
                </div>
            </div>
        )
    }
}

class RevisionOfFinancialDeparment extends React.Component<any,any> {

    constructor(props){
        super(props);

        this.state={
            REV_OPTIONS:[],
            RECOM_OPTIONS:[]
        }
        ObtenerTipoRevision().then(result=>this.setState({REV_OPTIONS : result}))
        ObtenerTipoRecomendacion().then(result=>this.setState({RECOM_OPTIONS : result}))
    }

    render() {
        return (
            <div className="container" style={{ paddingTop: "3%" }}>
                <Subtitulo nombre="V. Revision of Financial Department"></Subtitulo>
                <div className="row">
                    <div className="col-6 col-md-6">
                        <Label>01. Analyst:</Label>
                    </div>
                    <div className="col-6 col-md-6">
                        <TextField underlined onChanged={e => { this.setState({ RequestCode: e }); }} />
                    </div>
                </div>
                <div className="row">
                    <div className="col-6 col-md-6">
                        <Label>02. Manager:</Label>
                    </div>
                    <div className="col-6 col-md-6">
                    <TextField underlined onChanged={e => { this.setState({ RequestCode: e }); }} />
                    </div>
                </div>
                <div className="row">
                    <div className="col-6 col-md-6">
                        <Label>03. SOLMED:</Label>
                    </div>
                    <div className="col-6 col-md-6">
                        <TextField underlined onChanged={e => { this.setState({ RequestCode: e }); }} />
                    </div>
                </div>
                <div className="row">
                    <div className="col-6 col-md-6">
                        <Label>04. Investment Budget ($):</Label>
                    </div>
                    <div className="col-6 col-md-6">
                        <TextField value="2019" underlined onChanged={e => { this.setState({ RequestCode: e }); }} />
                    </div>
                </div>
                <div className="row">
                    <div className="col-6 col-md-6">
                        <Label>05. NPV ($):</Label>
                    </div>
                    <div className="col-6 col-md-6">
                        <TextField value="2019" underlined onChanged={e => { this.setState({ RequestCode: e }); }} />
                    </div>
                </div>
                <div className="row">
                    <div className="col-6 col-md-6">
                        <Label>06. Additional Revenues ($):</Label>
                    </div>
                    <div className="col-6 col-md-6">
                        <TextField value="2019" underlined onChanged={e => { this.setState({ RequestCode: e }); }} />
                    </div>
                </div>
                <div className="row">
                    <div className="col-6 col-md-6">
                        <Label>07. IRR (%):</Label>
                    </div>
                    <div className="col-6 col-md-6">
                        <TextField value="2019" underlined onChanged={e => { this.setState({ RequestCode: e }); }} />
                    </div>
                </div>
                <div className="row">
                    <div className="col-6 col-md-6">
                        <Label>08. Savings ($):</Label>
                    </div>
                    <div className="col-6 col-md-6">
                        <TextField value="2019" underlined onChanged={e => { this.setState({ RequestCode: e }); }} />
                    </div>
                </div>
                <div className="row">
                    <div className="col-6 col-md-6">
                        <Label>09. Payback ($):</Label>
                    </div>
                    <div className="col-6 col-md-6">
                        <TextField value="2019" underlined onChanged={e => { this.setState({ RequestCode: e }); }} />
                    </div>
                </div>
                <div className="row">
                    <div className="col-6 col-md-6">
                        <Label>10. Status of Revision:</Label>
                    </div>
                    <div className="col-6 col-md-6">
                        <ComboBox
                            options={this.state.REV_OPTIONS}
                        />
                    </div>
                </div>
                <div className="row">
                    <div className="col-6 col-md-6">
                        <Label>11. Recommendation:</Label>
                    </div>
                    <div className="col-6 col-md-6">
                        <ComboBox
                            options={this.state.RECOM_OPTIONS}
                        />
                    </div>
                </div>
                <div className="row">
                    <div className="col-6 col-md-6">
                        <Label>12. Approval Process:</Label>
                    </div>
                </div>
                <div className="row" style={{paddingLeft:"20px"}}>
                    <div className="col-12 col-md-12">
                        <table>
                            <tbody>
                                <tr>
                                    <td><Label>a. Investment committee</Label></td>
                                    <td>
                                        <ChoiceGroup styles={{ flexContainer: { display: "flex" } }}
                                            className="defaultChoiceGroup"
                                            options={[
                                                {
                                                    key: 'Approved',
                                                    text: 'Approved'
                                                },
                                                {
                                                    key: 'Rejected',
                                                    text: 'Rejected'
                                                }
                                            ]}
                                            required={true}
                                        />                                        
                                    </td>
                                    
                                </tr>
                                <tr>
                                    <td><Label>b. Management</Label></td>
                                    <td>
                                        <ChoiceGroup styles={{ flexContainer: { display: "flex" } }}
                                            className="defaultChoiceGroup"
                                            options={[
                                                {
                                                    key: 'Approved',
                                                    text: 'Approved'
                                                },
                                                {
                                                    key: 'Rejected',
                                                    text: 'Rejected'
                                                }
                                            ]}
                                            required={true}
                                        />                                        
                                    </td>
                                </tr>     
                                <tr>
                                    <td><Label>c. Excomm</Label></td>
                                    <td>
                                        <ChoiceGroup styles={{ flexContainer: { display: "flex" } }}
                                            className="defaultChoiceGroup"
                                            options={[
                                                {
                                                    key: 'Approved',
                                                    text: 'Approved'
                                                },
                                                {
                                                    key: 'Rejected',
                                                    text: 'Rejected'
                                                }
                                            ]}
                                            required={true}
                                        />
                                    </td>
                                    
                                </tr>                                                             
                            </tbody>
                        </table>
                    </div>                        
                </div>
            </div>
        )
    }
}

class Procurement extends React.Component {
    render() {
        return (
            <div className="container" style={{ paddingTop: "3%" }}>
                <Subtitulo nombre="VI. Procurement"></Subtitulo>
                <div className="row">
                    <div className="col-6 col-md-6">
                        <Label>01 .Maximum Amount of Investment ($):</Label>
                    </div>
                    <div className="col-6 col-md-6">
                        <TextField underlined onChanged={e => { this.setState({ RequestCode: e }); }} />
                    </div>
                </div>
                <div className="row">
                    <div className="col-6 col-md-6">
                        <Label>02. Amount of Investment from Tender Offer ($):</Label>
                    </div>
                    <div className="col-6 col-md-6">
                        <TextField underlined onChanged={e => { this.setState({ RequestCode: e }); }} />
                    </div>
                </div>
                <div className="row">
                    <div className="col-6 col-md-6">
                        <Label>03. Estimated award date(Tender Process):</Label>
                    </div>
                    <div className="col-6 col-md-6">
                        <DatePicker placeholder="Select a date..." ariaLabel="Select a date" />
                    </div>
                </div>
            </div>
        )
    }

}
const DocumentsArray =  [{DocumentType:"Technical scope",Descripcion:"Valores",UrlFolder:"URLDOC"},{DocumentType:"Technical scope",Descripcion:"Documento 2",UrlFolder:"URLDOC"}];
class Documents extends React.Component<any,any> {

    private _input:HTMLInputElement;

    constructor(props){
        super(props);
        this.state={
            Documents:[]            
        }
        
        
        ObtenerDocumentos('CR-TI-2019-0028').then(result=>this.setState({Documents : result}));
        
    }

    private uploadFileFromControl(){
        var files = this._input.files;
        var file = files[0];
        
        this.getFileBuffer(files).then((res:ArrayBuffer)=>{                       
            SubirDocumentos(res).then(result=>{console.log("listo")});
        });
    }


    private getFileBuffer(uploadedFiles) {
        let promised = new Promise((resolve, reject) => {
            let reader = new FileReader();
            reader.onload = (e) => {
                resolve(e.target.result)
            }
            reader.onerror = (e) => {
                reject(e.target.error);
            }
            reader.readAsArrayBuffer(uploadedFiles[0]);
        });
    
        return promised;
    }

    render() {
        return (
            <div>
                <div className="container" style={{ paddingTop: "3%" }}>
                    <Subtitulo nombre="VII. Documents"></Subtitulo>
                    <Paper style={{width: '100%',overflowX: 'auto'}} >
                        <Table style={{minWidth: 650}} aria-label="simple table">
                            <TableHead>
                            <TableRow>
                                <StyledTableCell >Document type</StyledTableCell>
                                <StyledTableCell align="left">Descripcion</StyledTableCell>
                                <StyledTableCell align="left">Url Folder</StyledTableCell>                                
                            </TableRow>
                            </TableHead>
                            <TableBody>
                            {this.state.Documents.map(row => (
                                <TableRow >
                                    <TableCell component="th" scope="row">
                                        {row.DocumentType}
                                    </TableCell>
                                    <TableCell align="left">{row.Description}</TableCell>
                                    <TableCell align="left"><a target='_blank' href={row.Url}>{row.NomDoc}</a></TableCell>
                                </TableRow>
                            ))}
                            </TableBody>
                        </Table>
                    </Paper> 
                    <div className="row">
                        <div className="col-6 col-md-6">
                            <TextField placeholder="DocumentType"  underlined onChanged={e => { this.setState({ RequestCode: e }); }} />
                        </div>
                        <div className="col-6 col-md-6">
                        <div className="">
                            <input type="file" ref={(elm) => { this._input = elm; }}></input>
                            <p>
                                <button onClick={() => this.uploadFileFromControl()} >
                                    Upload
                                </button>
                            </p>                            
                            </div>                                                        
                        </div>
                    </div>                    
                </div>
            </div>
        )
    }

}

interface propComment{
    User:string;
    Date:string;
    Comment:string;
    
}

interface IState {
    Comments: propComment[];
    newComment : string;
    CurrentUser: object[];
}
class Comments extends React.Component<any,IState> {
    
    state:IState={
        Comments:[],
        newComment:"",
        CurrentUser:[]
    };

    constructor(props){
        super(props);

    }

    componentDidMount(){



        ObtenerUsuarioActual().then(result=>this.setState({CurrentUser:result}));

        const rowCom = [{User:'dbalbin',Date:'29/10/2019',Comment:"Esto es un comentario"},{User:'dalarcon',Date:'29/10/2019',Comment:"Esto es un comentario 2"}];
        this.setState({
            Comments:rowCom
        });
    }

    addComment = e =>{
        const date = new Date();        
        const DateCurrenFormat = ('0'+(date.getDate())).slice(-2)+"/"+('0'+(date.getMonth()+1)).slice(-2)+"/"+date.getFullYear();

        var newComment = {User:this.state.CurrentUser["Title"],Date : DateCurrenFormat,Comment:this.state.newComment};
        this.setState({
            Comments:[...this.state.Comments,newComment],
            newComment:""
        }); 
    }

    render() {
        return (
            <div>
                <div className="container" style={{ paddingTop: "3%" }}>
                    <Subtitulo nombre="VIII. Comments"></Subtitulo>
                    <Paper style={{width: '100%',overflowX: 'auto'}} >
                        <Table style={{minWidth: 650}} aria-label="simple table">
                            <TableHead>
                            <TableRow>
                                <StyledTableCell >User</StyledTableCell>
                                <StyledTableCell align="left">Date</StyledTableCell>
                                <StyledTableCell align="left">Comment</StyledTableCell>                                
                            </TableRow>
                            </TableHead>
                            <TableBody>
                            {this.state.Comments.map(row => (
                                <TableRow >
                                    <TableCell component="th" scope="row">
                                        {row.User}
                                    </TableCell>
                                    <TableCell align="left">{row.Date}</TableCell>
                                    <TableCell align="left">{row.Comment}</TableCell>
                                </TableRow>
                            ))}
                            </TableBody>
                        </Table>
                    </Paper>    
                    <div className="row" style={{marginTop:10}}>                        
                        <div className="col-6 col-md-6">
                            <TextField underlined placeholder="Comment" defaultValue={this.state.newComment} onKeyPress={e=>{e.key=="Enter"?this.addComment(e):false}} onChanged={ e => {this.setState({newComment: e})}} />
                        </div>
                        <div className="col-2 col-md-2">
                            <IconButton onClick={this.addComment} iconProps={{iconName: 'Add'}} title="Plus" ariaLabel="Plus" />
                        </div>                        
                    </div>               
                </div>
            </div>
        )
    }

}

const StyledTableCell = withStyles((theme: Theme) =>
  createStyles({
    head: {
      backgroundColor: "#0078d4",
      color: theme.palette.common.white,
    },
    body: {
      fontSize: 14,
    },
  }),
)(TableCell);

class Tracking extends React.Component <any,any> {
    
      constructor(props){
          super(props); 

        
        Traking.push({status:"99",Username:"99",TaskDateCreated:"",DateOfCompletedTask:""});
        const valor = JSON.stringify(Traking);                        

      }
      
    render() {
        return (
            <div>
                <div className="container" style={{ paddingTop: "3%" }}>
                    <Subtitulo nombre="IX. Tracking"></Subtitulo>
                    <Paper style={{width: '100%',overflowX: 'auto'}} >
                        <Table style={{minWidth: 650}} aria-label="simple table">
                            <TableHead>
                            <TableRow>
                                <StyledTableCell >Status</StyledTableCell>
                                <StyledTableCell align="left">User Name</StyledTableCell>
                                <StyledTableCell align="left">Task date created</StyledTableCell>
                                <StyledTableCell align="left">Date of completed task</StyledTableCell>
                            </TableRow>
                            </TableHead>
                            <TableBody>
                            {rows.map(row => (
                                <TableRow key={row.Status}>
                                    <TableCell component="th" scope="row">
                                        {row.Status}
                                    </TableCell>
                                    <TableCell align="left">{row.UserName}</TableCell>
                                    <TableCell align="left">{row.TaskDateCreated}</TableCell>
                                    <TableCell align="left">{row.DateOfCompletedTask}</TableCell>
                                </TableRow>
                            ))}
                            </TableBody>
                        </Table>
                    </Paper>
                </div>
            </div>
        )
    }

}


export default function ScrollableTabsButtonAuto() {

    const classes = useStyles({});
    const [value, setValue] = React.useState(0);

    const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
        setValue(newValue);
    };

    return (
        <div className={classes.root}>
            <AppBar position="static" color="default">
                <Tabs
                    value={value}
                    onChange={handleChange}
                    indicatorColor="primary"
                    textColor="primary"
                    variant="scrollable"
                    scrollButtons="auto"
                    width="30px"
                    aria-label="scrollable auto tabs example"
                >
                    <Tab label="1" {...a11yProps(0)} />
                    <Tab label="2" {...a11yProps(1)} />
                    <Tab label="3" {...a11yProps(2)} />
                    <Tab label="4" {...a11yProps(3)} />
                    <Tab label="5" {...a11yProps(4)} />
                    <Tab label="6" {...a11yProps(5)} />
                    <Tab label="7" {...a11yProps(6)} />
                    <Tab label="8" {...a11yProps(7)} />
                    <Tab label="9" {...a11yProps(8)} />
                </Tabs>
            </AppBar>
            <TabPanel value={value} index={0}>
                <GeneralInformation ></GeneralInformation>
            </TabPanel>
            <TabPanel value={value} index={1}>
                <RequestInformation></RequestInformation>
            </TabPanel>
            <TabPanel value={value} index={2}>
                <ProposedSolutionForExecutionArea></ProposedSolutionForExecutionArea>
            </TabPanel>
            <TabPanel value={value} index={3}>
                <ExecutionSchedule></ExecutionSchedule>
            </TabPanel>
            <TabPanel value={value} index={4}>
                <RevisionOfFinancialDeparment></RevisionOfFinancialDeparment>
            </TabPanel>
            <TabPanel value={value} index={5}>
                <Procurement></Procurement>
            </TabPanel>
            <TabPanel value={value} index={6}>
                <Documents></Documents>
            </TabPanel>
            <TabPanel value={value} index={7}>
                <Comments></Comments>
            </TabPanel>
            <TabPanel value={value} index={8}>
                <Tracking></Tracking>
            </TabPanel>
        </div>
    );
}