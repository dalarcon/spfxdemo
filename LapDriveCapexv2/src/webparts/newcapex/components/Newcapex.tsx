import * as React from 'react';
import styles from './Newcapex.module.scss';
import { INewcapexProps } from './INewcapexProps';
import { escape } from '@microsoft/sp-lodash-subset';
import ReactWizard from "react-bootstrap-wizard";
import { Container, Row, Col } from "reactstrap";



import ScrollableTabsButtonAuto from './TabCapex';

import "bootstrap/dist/css/bootstrap.css";

class FirstStep extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      firstStep: "first step here"
    };
  }
  render() {
    return <div>Hey from First</div>;
  }
}
class SecondStep extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      secondStep: "second step here"
    };
  }
  isValidated() {
    // do some validations
    // decide if you will
    return true;
    // or you will
    // return false;
  }
  render() {
    return <div>Hey from Second</div>;
  }
}
class ThirdStep extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      thirdStep: "third step here"
    };
  }
  render() {
    return <div>Hey from Third</div>;
  }
}

var steps = [
  // this step hasn't got a isValidated() function, so it will be considered to be true
  { stepName: "1.Gerencia", component: FirstStep },
  // this step will be validated to false
  { stepName: "2.RQ", component: SecondStep },
  // this step will never be reachable because of the seconds isValidated() steps function that will always return false
  { stepName: "3.D", component: ThirdStep },
  { stepName: "4", component: ThirdStep },
  { stepName: "5", component: ThirdStep },
  { stepName: "6", component: ThirdStep },
  { stepName: "7", component: ThirdStep }
];


export default class Newcapex extends React.Component<INewcapexProps,any> {
  public finishButtonClick(allStates) {
    console.log(allStates);
  }
  public render(): React.ReactElement<INewcapexProps> {
    return (
    <Container fluid style={{ marginTop: "0px" }}>            
            <Row>
              <Col xs={12} md={12} className="mr-auto ml-auto">
                <ScrollableTabsButtonAuto></ScrollableTabsButtonAuto>
              </Col>
            </Row>
          </Container>
    );
  }
}
