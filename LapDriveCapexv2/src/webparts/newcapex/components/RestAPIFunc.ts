
import axios from 'axios';
import { string, object } from 'prop-types';


export async function ObtenerFuncionalidad(nivel:number):Promise<any>{
    
    var items = [];
    var ListName = "Nivel "+nivel+" SOLMED";
    var apiPath = "https://lapperu.sharepoint.com/sites/dkmtcapexqa/_api/web/lists/getByTitle('"+ListName+"')/items";
    
    const res =await axios.get(apiPath);
    return new Promise<any>((resolve,reject)=>{
        resolve(res.data.value);
    });    

}

export async function ObtenerTipoInversion():Promise<any>{
    
    var items = [];
    var ListName = "Tipos de inversión";
    var apiPath = "https://lapperu.sharepoint.com/sites/dkmtcapexqa/_api/web/lists/getByTitle('"+ListName+"')/items";
    
    const res =await axios.get(apiPath);
    return new Promise<any>((resolve,reject)=>{
        resolve(res.data.value.map(f => ({ key: f["ID"], text: f["Title"] })));
    });    

}

export async function ObtenerTipoCategoria():Promise<any>{
    
    var items = [];
    var ListName = "Categoría de inversión";
    var apiPath = "https://lapperu.sharepoint.com/sites/dkmtcapexqa/_api/web/lists/getByTitle('"+ListName+"')/items";
    
    const res =await axios.get(apiPath);
    return new Promise<any>((resolve,reject)=>{
        resolve(res.data.value.map(f => ({ key: f["ID"], text: f["Title"] })));
    });    

}

export async function ObtenerTipoRevision():Promise<any>{
    
    var items = [];
    var ListName = "Tipos de revisión";
    var apiPath = "https://lapperu.sharepoint.com/sites/dkmtcapexqa/_api/web/lists/getByTitle('"+ListName+"')/items";
    
    const res =await axios.get(apiPath);
    return new Promise<any>((resolve,reject)=>{
        resolve(res.data.value.map(f => ({ key: f["ID"], text: f["Title"] })));
    });    

}

export async function ObtenerTipoRecomendacion():Promise<any>{
    
    var items = [];
    var ListName = "Tipos de recomendación";
    var apiPath = "https://lapperu.sharepoint.com/sites/dkmtcapexqa/_api/web/lists/getByTitle('"+ListName+"')/items";
    
    const res =await axios.get(apiPath);
    return new Promise<any>((resolve,reject)=>{
        resolve(res.data.value.map(f => ({ key: f["ID"], text: f["Title"] })));
    });    

}

export async function ObtenerUsuarioActual():Promise<any>{
    
    var items = [];
    var ListName = "Tipos de recomendación";
    var apiPath = "https://lapperu.sharepoint.com/sites/dkmtcapexqa/_api/Web/CurrentUser";
    
    const res =await axios.get(apiPath);
    return new Promise<any>((resolve,reject)=>{
        resolve(res.data);
    });    

}

// document

export async function ObtenerDocumentos(Capex:string):Promise<any>{
    
    var items = [];
    var RelativeUrl = "docs/"+Capex;
    var apiPath = "https://lapperu.sharepoint.com/sites/dkmtcapexqa/2019/_api/Web/getfolderbyserverrelativeurl('"+RelativeUrl+"')/files?$expand=ListItemAllFields";
    
    const res =await axios.get(apiPath);
    return new Promise<any>((resolve,reject)=>{
        debugger;
        resolve(res.data.value.map(f => ({ 
            Url: f["ListItemAllFields"]["ServerRedirectedEmbedUri"] != null && f["Name"].indexOf(".pdf")==-1 ?f["ListItemAllFields"]["ServerRedirectedEmbedUri"].replace("action=interactivepreview", "action=view") :f["ServerRelativeUrl"],
            NomDoc: f["Name"], 
            Description: f["ListItemAllFields"]["Description0"] , 
            DocumentType: f["ListItemAllFields"]["DocumentType"] })));
    });    

}

// subir documentos
export async function SubirDocumentos(file:ArrayBuffer):Promise<any>{
    /*
    axios.post(SiteUrlUpdated, spCreateQuery)
    .then((response) => {
        let item = response.data.d.Id;
        //transform the file
        getFileBuffer(uploadedFiles).then(() => {
            //upload the file 
            axiosApi.post(SiteUrl + "(" + item + ")/AttachmentFiles/add(FileName='" + uploadedFiles[0].name + "')", uploadedFiles[0], headerConfig).then((response) => {
                console.log(uploadedFiles[0]);
                console.log("Uploaded file");
                console.log(response);
            })
        })
    })
    */
   console.log(file.byteLength);

   var RelativoPath = "docs/CR-TI-2019-0028";
   var apiPath = "https://lapperu.sharepoint.com/sites/dkmtcapexqa/2019//_api/web/getfolderbyserverrelativeurl('"+RelativoPath+"')/files/add(overwrite=true, url='Reporte N° 22222.docx')";
   axios({
    method: 'post',
    url: apiPath,    
    headers:{
        "accept": "application/json;odata=verbose"
        
    },
    data: file
  });

   
    /*
    const res =await axios.post(apiPath,file);    

    
    return new Promise<any>((resolve,reject)=>{        
        resolve("Terminó!!!!!!!!");
    });    
    */
}

