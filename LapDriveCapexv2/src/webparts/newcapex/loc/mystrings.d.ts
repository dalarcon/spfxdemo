declare interface INewcapexWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
}

declare module 'NewcapexWebPartStrings' {
  const strings: INewcapexWebPartStrings;
  export = strings;
}
