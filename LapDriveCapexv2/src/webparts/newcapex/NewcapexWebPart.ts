import * as React from 'react';
import * as ReactDom from 'react-dom';
import { Version } from '@microsoft/sp-core-library';
import {
  BaseClientSideWebPart,
  IPropertyPaneConfiguration,
  PropertyPaneTextField
} from '@microsoft/sp-webpart-base';

import * as strings from 'NewcapexWebPartStrings';
import Newcapex from './components/Newcapex';
import { INewcapexProps } from './components/INewcapexProps';

export interface INewcapexWebPartProps {
  description: string;
}

export default class NewcapexWebPart extends BaseClientSideWebPart<INewcapexWebPartProps> {

  public render(): void {
    console.log(this.context.pageContext.web.absoluteUrl);
    const element: React.ReactElement<INewcapexProps > = React.createElement(
      Newcapex,
      {
        description: this.properties.description
      }
    );

    ReactDom.render(element, this.domElement);
  }

  protected onDispose(): void {
    ReactDom.unmountComponentAtNode(this.domElement);
  }

  protected get dataVersion(): Version {
    return Version.parse('1.0');
  }

  protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {
    return {
      pages: [
        {
          header: {
            description: strings.PropertyPaneDescription
          },
          groups: [
            {
              groupName: strings.BasicGroupName,
              groupFields: [
                PropertyPaneTextField('description', {
                  label: strings.DescriptionFieldLabel
                })
              ]
            }
          ]
        }
      ]
    };
  }
}
