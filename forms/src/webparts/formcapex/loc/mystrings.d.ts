declare interface IFormcapexWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
}

declare module 'FormcapexWebPartStrings' {
  const strings: IFormcapexWebPartStrings;
  export = strings;
}
