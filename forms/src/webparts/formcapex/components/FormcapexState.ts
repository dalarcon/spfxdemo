import { IPickerTerms } from "@pnp/spfx-controls-react/lib/TaxonomyPicker";
export interface IFormcapexState {  
    UsersId: string[];
    LoginUsersEdit:string[];
    selectedTerms: IPickerTerms;
    RequestCode : string;
    DateofRequest : Date;
    Status : string;  
    Year : string;
    RequestApprovedBudget : string;
    DeadlineResult: string;
    terms:IPickerTerms;
    idItem:number;
    
} 