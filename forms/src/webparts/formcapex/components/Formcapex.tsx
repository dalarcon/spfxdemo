import * as React from 'react';
import styles from './Formcapex.module.scss';
import { escape } from '@microsoft/sp-lodash-subset';

import { TextField, MaskedTextField } from 'office-ui-fabric-react/lib/TextField';
import { Depths } from '@uifabric/fluent-theme/lib/fluent/FluentDepths';
import { IFormcapexProps } from './IFormcapexProps';
import { IFormcapexState } from './FormcapexState';

// @pnp/sp imports  
import { sp,RenderListDataParameters  } from '@pnp/sp';  
import { PeoplePicker, PrincipalType } from "@pnp/spfx-controls-react/lib/PeoplePicker"; 
import { TaxonomyPicker, IPickerTerms,IPickerTerm } from "@pnp/spfx-controls-react/lib/TaxonomyPicker";
import { getGUID } from "@pnp/common";

// Import button component    
import { DefaultButton } from 'office-ui-fabric-react/lib/Button'; 
import { autobind } from 'office-ui-fabric-react'; 
import { ChoiceGroup, IChoiceGroupOption } from 'office-ui-fabric-react/lib/ChoiceGroup';
import { Label } from 'office-ui-fabric-react/lib/Label';
import { Stack, IStackProps } from 'office-ui-fabric-react/lib/Stack';
import { DatePicker, DayOfWeek, IDatePickerStrings } from 'office-ui-fabric-react';

import {
  CompactPeoplePicker,
  IBasePickerSuggestionsProps,
  IBasePicker,
  ListPeoplePicker,
  NormalPeoplePicker,
  ValidationState
} from 'office-ui-fabric-react/lib/Pickers';


export class Subtitulo extends React.Component<any,any>{

  public render(){
    return(
      <div className={styles.subTitle} style={{ boxShadow: Depths.depth64 }} >
        <h3>{this.props.nombre}</h3>
      </div>
    );
  }
}


export default class Formcapex extends React.Component<IFormcapexProps,IFormcapexState> {

    constructor(props: IFormcapexProps, state: IFormcapexState) {
      super(props);    

      let idItemUpt = parseInt(this.props.idItem);

      this.state = {
        UsersId: [],
        selectedTerms:[],
        RequestCode: "",
        DateofRequest: null,
        Status:"",
        Year:"",
        RequestApprovedBudget:"",
        DeadlineResult:"",
        terms:[],
        LoginUsersEdit:[] ,
        idItem:-1        
      };      

       
      

      if(idItemUpt!=-1){
        sp.web.lists.getByTitle("Demos").items.getById(idItemUpt)
        .select("*,Users/EMail")
        .expand('Users').get().then((item: any) => { 
          console.log(item);   
          var usuarios = item["Users"]!=null? item["Users"].map((u:any)=>{return u.EMail}):[];
          var taxonomyObject = item["FieldTaxonomy"].map((t:any)=>
          {return {
            key:t.TermGuid,
            name:t.Label,
            path:"",
            termSet:""
          }          
          });
          
          this.setState({RequestCode:item["RequestCode"]});
          this.setState({DateofRequest: new Date(item["DateofRequest"])});
          this.setState({Status:item["Status"]});
          this.setState({Year:item["Year"]});
          this.setState({RequestApprovedBudget:item["RequestApprovedBudget"]});
          this.setState({DeadlineResult:item["DeadlineResult"]});
          this.setState({UsersId:item["UsersId"]});
          this.setState({terms:taxonomyObject});                  
          this.setState({LoginUsersEdit:usuarios})
          this.setState({idItem:idItemUpt});          

          console.log(this.state);

        });        
      }


    }


  
    

    public render(): React.ReactElement<IFormcapexProps> {
      return (
        <div className={ styles.formcapex }>
          <div className={ styles.container }>

          <Subtitulo nombre="I. General Information"></Subtitulo>
            <div className={ styles.row }>            
              <div className={styles.md3}>
              <Label> 01. Request Code:</Label>                              
              </div>
              <div className={styles.md3}>
              <TextField  underlined  defaultValue={this.state.RequestCode} onChanged={ e => {this.setState({RequestCode: e});} } />  
              </div>
            </div>
            <div className={ styles.row }>            
              <div className={styles.md3}>
              <Label> 02. Date of Request:</Label>                              
              </div>
              <div className={styles.md3}>
                <DatePicker value={this.state.DateofRequest} onSelectDate={this._onSelectDate}  placeholder="Select a date..." ariaLabel="Select a date" />
              </div>
              <div className={styles.md6}>
                <ChoiceGroup  styles={{ flexContainer: { display: "flex" } }}
                    className="defaultChoiceGroup"                    
                    selectedKey={this.state.DeadlineResult}
                    options={[
                      {
                        key: 'In date',
                        text: 'In date'
                      },
                      {
                        key: 'Out of date',
                        text: 'Out of date'                      
                      }
                    ]}                                    
                    required={true}
                    onChange={this.CambiarEstadoRadioDLR}
                  />
              </div>            
            </div>
          
            <div className={ styles.row }>            
              <div className={styles.md3}>
              <Label>  03. Status:</Label>                              
              </div>
              <div className={styles.md3}>
              <TextField  underlined  defaultValue={this.state.Status} onChanged={ e => {this.setState({Status: e});} } />  
              </div>
            </div>   
            <div className={ styles.row }>            
              <div className={styles.md3}>
              <Label>  04. Year of Investment Program:</Label>                              
              </div>
              <div className={styles.md3}>
              <TextField  underlined  defaultValue={this.state.Year}  onChanged={ e => {this.setState({Year: e});} }/>
              </div>
            </div>  
            <div className={ styles.row }>            
              <div className={styles.md3}>
              <Label>  05. The request has an approved Budget in the Investment Plan:</Label> 
              </div>
              <div className={styles.md4}>
                <ChoiceGroup  styles={{ flexContainer: { display: "flex", paddingLeft:20 } }}
                    className="defaultChoiceGroup"                                        
                    selectedKey={this.state.RequestApprovedBudget}
                    options={[
                      {
                        key: 'Yes',
                        text: 'Yes'
                      },
                      {
                        key: 'No',
                        text: 'No'                      
                      }
                    ]}                                     
                    required={true}
                    onChange={this.CambiarEstadoRadioRAB}
                    
                  />
              </div>  
            </div>           
                
            <div>    
                <Subtitulo nombre="II. Request Information"></Subtitulo>
                <PeoplePicker
                  context={this.props.context}
                  titleText="Personas:"
                  
                  personSelectionLimit={10}
                  groupName={""} 
                  showtooltip={true}
                  isRequired={true}                  
                  defaultSelectedUsers={this.state.LoginUsersEdit}
                  disabled={false}
                  ensureUser={true}                                    
                  selectedItems={this._getPeoplePickerItems}
                  showHiddenInUI={false}
                  principalTypes={[PrincipalType.User]}
                  resolveDelay={1000} /> 

                <TaxonomyPicker allowMultipleSelections={true}
                  termsetNameOrID="Concursos"
                  panelTitle="Select Term"
                  label="Taxonomy Picker"      
                  initialValues={this.state.terms}
                  context={this.props.context}
                  onChange={this.onTaxPickerChange}
                  isTermSetSelectable={false} />                  
                  
                <DefaultButton  
                  data-automation-id="addSelectedUsers"  
                  title="Add Selected Users"  
                  onClick={this.addSelectedUsers}>  
                  Add Selected Users  
                </DefaultButton>   
                
              </div>
          </div>
        </div>
      );
    }

    
    private _onSelectDate = (date: Date | null | undefined): void => {
      this.setState({ DateofRequest: date });
    };
  

    @autobind
    private CambiarEstadoRadioDLR(ev: React.FormEvent<HTMLInputElement>, option: IChoiceGroupOption) {
      console.log(option);
      this.setState({DeadlineResult:option.key});
    }

    @autobind
    private CambiarEstadoRadioRAB(ev: React.FormEvent<HTMLInputElement>, option: IChoiceGroupOption) {
      console.log(option);
      this.setState({RequestApprovedBudget:option.key});
    }
    
    @autobind   
    private onTaxPickerChange(terms : IPickerTerms) {  
      console.log("Terms", terms);  
      this.setState({ selectedTerms: terms });  
    }

    @autobind 
    private _getPeoplePickerItems(items: any[]) {
      console.log('Items:', items);

      let selectedUsers = [];
      for (let item in items) {
        selectedUsers.push(items[item].id);
      }

      this.setState({ UsersId: selectedUsers });
    } 
  
    private obtenerLoginName (idUser:number): string{

      sp.web.siteUsers.getById(idUser).get().then((result)=> {
        console.log(result);
      });
      return "";
    }


    @autobind   
    private addSelectedUsers(): void { 
      
      /*
      var objTerm= [{
        key: "7926f17a-c200-4e70-915b-f4c6d2b189e0",
        name: "2. En proceso",
        path: "GC Comercial;Estado;2. En proceso",
        termSet: "f20bbd42-b007-42e9-90ad-b54e9677ecf9",
      },
      {
        key: "d76dbbb1-2113-4ba0-8e8b-84f256bb3f34",
        name: "Privado",
        path: "GC Comercial;Tipo;Privado",
        termSet: "f20bbd42-b007-42e9-90ad-b54e9677ecf9",
      }
      ];
  {
          __metadata: { "type": "SP.Taxonomy.TaxonomyFieldValue" },
          Label: "Privado",
          TermGuid: "d76dbbb1-2113-4ba0-8e8b-84f256bb3f34",
          WssId: -1
        }
        */

/*
      console.log(this.state.UsersId);
      sp.web.lists.getByTitle("Demos").items.add({  
        Title: getGUID(),
        UsersId: {   
          results: this.state.UsersId
        },
        FieldTaxonomy_0: "-1;#2. En proceso|f20bbd42-b007-42e9-90ad-b54e9677ecf9;#-1;#Privado|f20bbd42-b007-42e9-90ad-b54e9677ecf9"
      }).then(i => {  
          console.log(i);  
      });  

*/
      const spfxList = sp.web.lists.getByTitle('Demos');    
  


      

      // If the name of your taxonomy field is SomeMultiValueTaxonomyField, the name of your note field will be SomeMultiValueTaxonomyField_0  
      const multiTermNoteFieldName = 'FieldTaxonomy_0';  
      
     
      
      let termsString: string = '';
      this.state.selectedTerms.forEach(term => {  
        termsString += `-1;#${term.name}|${term.key};#`;  
      });  
      
      spfxList.getListItemEntityTypeFullName()  
        .then((entityTypeFullName) => {  
          spfxList.fields.getByTitle(multiTermNoteFieldName).get()  
            .then((taxNoteField) => {  
              const multiTermNoteField = taxNoteField.InternalName;  
              const updateObject = {  
                Title: "item"+this.state.idItem,
                UsersId: {   
                  results: this.state.UsersId
                },
                RequestCode : this.state.RequestCode,
                Status : this.state.Status,
                Year : this.state.Year,
                RequestApprovedBudget: this.state.RequestApprovedBudget,
                DeadlineResult : this.state.DeadlineResult,
                DateofRequest : this.state.DateofRequest
              };  
              updateObject[multiTermNoteField] = termsString;  
              console.log("Envio de datos !");
              console.log(updateObject);

              /*
              spfxList.items.add(updateObject, entityTypeFullName)  
                .then((updateResult) => {  
                    console.dir(updateResult);  
                })  
                .catch((updateError) => {  
                    console.dir(updateError);  
                });  
                
                */               
              spfxList.items.getById(this.state.idItem).update(updateObject).then(i => {
                  console.log(i);
              });
/*
               spfxList.items.getById(this.state.idItem).update(updateObject, entityTypeFullName)  
               .then((updateResult) => {  
                   console.dir(updateResult);  
               })  
               .catch((updateError) => {  
                   console.dir(updateError);  
               }); 

               */
            });  
        });       


    }
    
    


}
