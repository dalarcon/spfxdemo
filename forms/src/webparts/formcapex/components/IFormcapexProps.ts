import { WebPartContext } from '@microsoft/sp-webpart-base';


export interface IFormcapexProps {
  description: string;
  context: WebPartContext;    
  idItem: string;

}
