import * as React from 'react';
import * as ReactDom from 'react-dom';
import { Version,UrlQueryParameterCollection } from '@microsoft/sp-core-library';
import { BaseClientSideWebPart } from '@microsoft/sp-webpart-base';
import {
  IPropertyPaneConfiguration,
  PropertyPaneTextField
} from '@microsoft/sp-property-pane';

import * as strings from 'FormcapexWebPartStrings';
import Formcapex from './components/Formcapex';
import { IFormcapexProps } from './components/IFormcapexProps';

import { sp,RenderListDataParameters  } from '@pnp/sp';  

export interface IFormcapexWebPartProps {
  description: string;  
}

export default class FormcapexWebPart extends BaseClientSideWebPart<IFormcapexWebPartProps> {

  protected obtenerItemId():string{
    var queryParms = new UrlQueryParameterCollection(window.location.href);
    if(queryParms.getValue("itemId")){
        return queryParms.getValue("itemId");
    }else{
      return "-1";
    }
    
  }

  protected onInit(): Promise<void> {

    sp.setup({
      spfxContext: this.context
    });
    return super.onInit();


  }

  public render(): void {
    const element: React.ReactElement<IFormcapexProps > = React.createElement(
      Formcapex,
      {
        description: this.properties.description,
        context: this.context,
        idItem: this.obtenerItemId()         
      }
    );



    ReactDom.render(element, this.domElement);
  }

  protected onDispose(): void {
    ReactDom.unmountComponentAtNode(this.domElement);
  }

  protected get dataVersion(): Version {
    return Version.parse('1.0');
  }

  protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {
    return {
      pages: [
        {
          header: {
            description: strings.PropertyPaneDescription
          },
          groups: [
            {
              groupName: strings.BasicGroupName,
              groupFields: [
                PropertyPaneTextField('description', {
                  label: strings.DescriptionFieldLabel
                })
              ]
            }
          ]
        }
      ]
    };
  }
}
