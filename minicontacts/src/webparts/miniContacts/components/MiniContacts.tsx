import * as React from 'react';
import {Component} from 'react';
import styles from './MiniContacts.module.scss';
import { IMiniContactsProps } from './IMiniContactsProps';
import { escape } from '@microsoft/sp-lodash-subset';

import { 
  IPersonaSharedProps,
  Persona
 } from 'office-ui-fabric-react/lib/Persona';

export class ContactsCard extends Component<any,any>{

  public render(){
    return (      
        <div className="ms-Grid">
          <div className="ms-Grid-row">
            <div className="ms-Grid-col ms-sm12 ms-md4">
              <Persona
              imageUrl="TestImages.personaFemale"
              imageInitials="AL"
              text= 'Annie Lindqvist'
              secondaryText= 'Software Engineer'
              tertiaryText ='In a meeting'
              optionalText = 'Available at 4:00pm'
              />
            </div>
            <div className="ms-Grid-col ms-sm12 ms-md4">
              <Persona
              imageUrl="TestImages.personaFemale"
              imageInitials="AL"
              text= 'Annie Lindqvist'
              secondaryText= 'Software Engineer'
              tertiaryText ='In a meeting'
              optionalText = 'Available at 4:00pm'
              />
            </div>
            <div className="ms-Grid-col ms-sm12 ms-md4">
              <Persona
              imageUrl="TestImages.personaFemale"
              imageInitials="AL"
              text= 'Annie Lindqvist'
              secondaryText= 'Software Engineer'
              tertiaryText ='In a meeting'
              optionalText = 'Available at 4:00pm'
              />
            </div>                        
          </div>        
        </div>
    );
  }
}


export default class MiniContacts extends React.Component<IMiniContactsProps, {}> {
  public render(): React.ReactElement<IMiniContactsProps> {
    return (
      <div className={ styles.miniContacts }>
        <div className={ styles.container }>
          <h1>Mini contacts</h1>
          <ContactsCard/>

        </div>
      </div>
    );
  }
}
