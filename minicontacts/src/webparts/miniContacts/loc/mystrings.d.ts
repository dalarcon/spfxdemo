declare interface IMiniContactsWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
}

declare module 'MiniContactsWebPartStrings' {
  const strings: IMiniContactsWebPartStrings;
  export = strings;
}
